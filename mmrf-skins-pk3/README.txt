======================================================================================
									Mega Man Rock Force
======================================================================================

The original fangame was a creation by GoldWaterDLS. Check out the official site!
http://megamanrockforce.com/rockforcemain.html
And the credits for the original fangame
http://megamanrockforce.com/rockforcecredits.html

X===============X
| Skin  Credits |
X===============X

All bot chats were written by Maxine / Yellow Devil

Crypt Man - FTX6004
Photon Man - FTX6004
Pulse Man - kirbymariomega
Virus Man - Raze
Boom Man - Dimpsy
Shock Man - Bass44
Circuit Man - FTX6004
Charade Man - Dimpsy

Terror Man - Brash Buster (+others idk)
Plague Man - Blaze/Dimpsy/FTX6004
Power Man - Bass44/Dimpsy?/FTX6004

Thrill Man - Dimpsy
Thermo Man - FTX6004
War Man - FTX6004 (+Dimpsy?)
Port Man - JaxOf7

Death Man - FTX6004
Fish Man - FTX6004
Justice Man - Maxine / Yellow Devil
